package com.patientmanager.patients;

import android.graphics.Bitmap;

/**
 * Created by Louis on 24/02/2016.
 */
public class Patient {

    int id;
    String name;
    String number;
    String dob;
    String address;
    String nhsNumber;
    String bio;
    Bitmap photo;

    public Patient() {

    }

    public Patient(int id, String name, String number, Bitmap photo) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.photo = photo;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNhsNumber() {
        return nhsNumber;
    }

    public void setNhsNumber(String nhsNumber) {
        this.nhsNumber = nhsNumber;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }
}
