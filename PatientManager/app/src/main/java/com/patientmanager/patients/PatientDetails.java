package com.patientmanager.patients;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;
import com.patientmanager.notes.PatientNotes;
import com.patientmanager.notes.StaffReminders;

/**
 * Created by Louis on 24/02/2016.
 */
public class PatientDetails extends Fragment {

    private int patientId;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private TextView name, bio, dob, address, number, nhsNumber;
    private ImageView image;
    private Button edit, delete, notes, reminders;
    private Patient patient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.patient_details_screen, container, false);

        name = (TextView) view.findViewById(R.id.name);
        bio = (TextView) view.findViewById(R.id.bio);
        dob = (TextView) view.findViewById(R.id.dob);
        address = (TextView) view.findViewById(R.id.address);
        number = (TextView) view.findViewById(R.id.number);
        nhsNumber = (TextView) view.findViewById(R.id.nhsNumber);
        image = (ImageView) view.findViewById(R.id.patientImage);

        bio.setMovementMethod(new ScrollingMovementMethod());
        address.setMovementMethod(new ScrollingMovementMethod());

        patient = loadPatientById(patientId);
        addDetailsToView(patient);

        edit = (Button) view.findViewById(R.id.editBtn);
        delete = (Button) view.findViewById(R.id.deleteBtn);
        notes = (Button) view.findViewById(R.id.notesBtn);
        reminders = (Button) view.findViewById(R.id.remindersBtn);

        setButtonHandlers();

        return view;
    }

    private Patient loadPatientById(int id) {

        Patient patient = new Patient();

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery = "SELECT * FROM patients where patientId = " + id;

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            int patientId = cursorData.getInt(cursorData.getColumnIndex("patientId"));
            String name = cursorData.getString(cursorData.getColumnIndex("name"));
            String number = cursorData.getString(cursorData.getColumnIndex("number"));
            String dob = cursorData.getString(cursorData.getColumnIndex("dob"));
            String address = cursorData.getString(cursorData.getColumnIndex("address"));
            String nhsNumber = cursorData.getString(cursorData.getColumnIndex("nhs_number"));
            String bio = cursorData.getString(cursorData.getColumnIndex("bio"));
            byte[] photoByte = cursorData.getBlob(cursorData.getColumnIndex("photo"));
            Bitmap photo = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);

            /*Set variables on patient*/
            patient.setId(patientId);
            patient.setName(name);
            patient.setNumber(number);
            patient.setDob(dob);
            patient.setAddress(address);
            patient.setNhsNumber(nhsNumber);
            patient.setBio(bio);
            patient.setPhoto(photo);
        }

        cursorData.close();
        database.close();
        return patient;
    }

    private void addDetailsToView(Patient patient) {
        name.setText(patient.getName());
        bio.setText(patient.getBio());
        dob.setText(patient.getDob());
        address.setText(patient.getAddress());
        number.setText(patient.getNumber());
        nhsNumber.setText(patient.getNhsNumber());
        image.setImageBitmap(patient.getPhoto());

        //set content desciptions for talkback
        name.setContentDescription(name.getText());
        bio.setContentDescription(bio.getText());
        dob.setContentDescription(dob.getText());
        address.setContentDescription(address.getText());
        number.setContentDescription(number.getText());
        nhsNumber.setContentDescription(nhsNumber.getText());
    }

    private void setButtonHandlers() {

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddEditPatient addEditPatient = new AddEditPatient();
                addEditPatient.setEditingPatient(patient);

                ((MainActivity) getActivity()).updateFragment(addEditPatient);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().setTheme(R.style.PreferencesTheme);
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

                dialog.setTitle("Delete Patient");
                dialog.setMessage("Are you sure you want to delete this patient?");
                dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().setTheme(R.style.AppTheme);
                        deletePatient();
                    }
                });
                dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().setTheme(R.style.AppTheme);
                    }
                });
                dialog.show();
            }
        });

        notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PatientNotes notes = new PatientNotes();
                notes.setPatientDetails(patientId, patient.getName());
                ((MainActivity) getActivity()).updateFragment(notes);
            }
        });

        reminders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaffReminders staffReminders = new StaffReminders();
                staffReminders.setPatientDetails(patientId, patient.getName());
                ((MainActivity) getActivity()).updateFragment(staffReminders);
            }
        });
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    private void deletePatient() {
        database = databaseHandler.getReadableDatabase();
        database.delete("patients", "patientId=" + patient.getId(), null);

        //delete all associated notes, reminders and tasks
        database.delete("notes", "patientId=" + patient.getId(), null);
        database.delete("reminders", "patientId=" + patient.getId(), null);
        database.delete("planner_items", "patientId=" + patient.getId(), null);

        databaseHandler.close();

        Toast.makeText(getActivity(), "Patient Deleted", Toast.LENGTH_SHORT).show();
        ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
    }

    public void backButtonPressed() {
        ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
    }
}
