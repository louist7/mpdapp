package com.patientmanager.patients;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Louis on 24/02/2016.
 */
public class AddEditPatient extends Fragment {

    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private Button save, cancel;
    private ImageButton takePhoto;
    private EditText name, number, dob, address, nhsNumber, bio, photo;
    private Patient patient;
    private Boolean editing = false;
    private Calendar calendar;
    private SimpleDateFormat dateFormatter;
    private static final int PHOTO_REQUEST_CODE = 100;
    private static final int PHOTO_TAKE_CODE = 1;
    private Bitmap patientPhoto;
    private Drawable originalBackground;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_edit_patient, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        name = (EditText) view.findViewById(R.id.patientName);
        number = (EditText) view.findViewById(R.id.patientNumber);
        dob = (EditText) view.findViewById(R.id.patientDob);
        address = (EditText) view.findViewById(R.id.patientAddress);
        nhsNumber = (EditText) view.findViewById(R.id.patientNhsNumber);
        bio = (EditText) view.findViewById(R.id.patientBio);
        photo = (EditText) view.findViewById(R.id.patientPhoto);

        originalBackground = name.getBackground();

        save = (Button) view.findViewById(R.id.saveBtn);
        cancel = (Button) view.findViewById(R.id.cancelBtn);

        calendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

        takePhoto = (ImageButton) view.findViewById(R.id.takePhoto);

        setListeners();
        renderDetailsOnScreen();

        return view;
    }

    private void renderDetailsOnScreen() {

        if (editing) {
            name.setText(patient.getName());
            number.setText(patient.getNumber());
            address.setText(patient.getAddress());
            nhsNumber.setText(patient.getNhsNumber());
            bio.setText(patient.getBio());

            patientPhoto = patient.getPhoto();
            photo.setHint("Update");

            String dobTemp = patient.getDob();
            int day = Integer.valueOf(dobTemp.substring(0, 2));
            int month = Integer.valueOf(dobTemp.substring(3, 5))-1;
            int year = Integer.valueOf(dobTemp.substring(6, 10));
            calendar.set(year, month, day);
            dob.setText(dateFormatter.format(calendar.getTime()));
        } else {
            photo.setHint("Choose");
            Drawable drawable = getResources().getDrawable(R.drawable.person);
            patientPhoto = ((BitmapDrawable)drawable).getBitmap();

            calendar.set(1970, 0, 01);
            dob.setText(dateFormatter.format(calendar.getTime()));
        }
    }

    private void setListeners() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).hideKeyboard();

                if (validateInput()) {

                    database = databaseHandler.getReadableDatabase();

                    ContentValues contentValues = new ContentValues();

                    contentValues.put("name", name.getText().toString());
                    contentValues.put("number", number.getText().toString());
                    contentValues.put("dob", dob.getText().toString());
                    contentValues.put("address", address.getText().toString());
                    contentValues.put("nhs_number", nhsNumber.getText().toString());
                    contentValues.put("bio", bio.getText().toString());
                    contentValues.put("photo", prepareImage());

                    if (editing) {
                        database.update("patients", contentValues, "patientId=" + patient.getId(), null);
                    } else {
                        database.insert("patients", null, contentValues);
                    }

                    databaseHandler.close();

                    Toast.makeText(getActivity(), "Patient Details Saved", Toast.LENGTH_SHORT).show();
                    if (editing) {
                        PatientDetails patientDetails = new PatientDetails();
                        patientDetails.setPatientId(patient.getId());
                        ((MainActivity) getActivity()).updateFragment(patientDetails);
                    } else {
                        ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).hideKeyboard();
                if (!editing) {
                    ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
                } else {
                    PatientDetails patientDetails = new PatientDetails();
                    patientDetails.setPatientId(patient.getId());
                    ((MainActivity) getActivity()).updateFragment(patientDetails);
                }
            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().setTheme(R.style.PreferencesTheme);
                DatePickerDialog dateDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int mon, int day) {
                        calendar.set(year, mon, day);
                        dob.setText(dateFormatter.format(calendar.getTime()));
                        getActivity().setTheme(R.style.AppTheme);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                dateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().setTheme(R.style.AppTheme);
                    }
                });
                dateDialog.show();
            }
        });

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, PHOTO_REQUEST_CODE);
            }
        });

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, 1);
                }
            }
        });
    }

    public void backButtonPressed() {
        if (!editing) {
            ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
        } else {
            PatientDetails patientDetails = new PatientDetails();
            patientDetails.setPatientId(patient.getId());
            ((MainActivity) getActivity()).updateFragment(patientDetails);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (requestCode == PHOTO_REQUEST_CODE || requestCode == PHOTO_TAKE_CODE) {
            try {
                Uri selectedImage = imageReturnedIntent.getData();

                Context context = getActivity().getApplicationContext();
                InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 6;

                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream, null, options);

                int rotation = getPhotoOrientation(context, selectedImage);

                Matrix matrix = new Matrix();
                matrix.postRotate(rotation);
                yourSelectedImage  = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(), yourSelectedImage.getHeight(), matrix, true);

                setImageBitmap(yourSelectedImage);
            } catch(Exception e) {
                //problem reading file
            }
        }
    }

    public int getPhotoOrientation(Context context, Uri imageUri){

        int orientation = 0;

        String[] orientationColumn = {MediaStore.Images.Media.ORIENTATION};
        Cursor cur = context.getContentResolver().query(imageUri, orientationColumn, null, null, null);

        if (cur != null && cur.moveToFirst()) {
            orientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
        }

        return orientation;
    }

    private void setImageBitmap(Bitmap image) {
        this.patientPhoto = image;
        photo.setHint("Update");
    }

    private byte[] prepareImage() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        patientPhoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    private Boolean validateInput() {

        changeBackground(name);
        changeBackground(number);
        changeBackground(dob);
        changeBackground(address);
        changeBackground(nhsNumber);
        changeBackground(bio);

        return (name.getText().length() > 0) &&
                (number.getText().length() > 0) &&
                (dob.getText().length() > 0) &&
                (address.getText().length() > 0) &&
                (nhsNumber.getText().length() > 0) &&
                (bio.getText().length() > 0);
    }

    private void changeBackground(EditText editText) {
        if (editText.getText().length() > 0) {
            editText.setBackground(originalBackground);
        } else {
            editText.setBackground(getResources().getDrawable(R.drawable.edittext_error));
        }
    }

    public void setEditingPatient(Patient details) {
        patient = details;
        editing = true;
    }
}