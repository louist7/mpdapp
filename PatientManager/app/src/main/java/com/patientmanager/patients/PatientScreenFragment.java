package com.patientmanager.patients;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Louis on 22/02/2016.
 */
public class PatientScreenFragment extends Fragment {

    private TableLayout baseTable;
    private TableRow.LayoutParams doublePatients, singlePatient;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private Button addPatient;
    private EditText search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.patient_screen, container, false);

        baseTable = (TableLayout) view.findViewById(R.id.baseTable);

        doublePatients = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        doublePatients.setMargins(5, 5, 5, 5);

        singlePatient = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        singlePatient.setMargins(5, 5, 5, 5);

        addPatientsToLayout(readPatientsFromDB(null));

        addPatient = (Button) view.findViewById(R.id.addPatientBtn);

        addPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).updateFragment(new AddEditPatient());
            }
        });

        search = (EditText) view.findViewById(R.id.searchPatients);

        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    ((MainActivity) getActivity()).hideKeyboard();
                    addPatientsToLayout(readPatientsFromDB((v.getText()).toString()));
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private List<Patient> readPatientsFromDB(String filter) {

        List<Patient> patients = new ArrayList<Patient>();

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery;

        if (filter == null) {
            selectQuery = "SELECT * FROM patients";
        } else {
            removePatientsFromBaseView();
            selectQuery = "SELECT * FROM patients where name like '%"+filter+"%'";
        }

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            while (!cursorData.isAfterLast()) {

                int id = cursorData.getInt(cursorData.getColumnIndex("patientId"));
                String name = cursorData.getString(cursorData.getColumnIndex("name"));
                String number = cursorData.getString(cursorData.getColumnIndex("number"));

                byte[] photoByte = cursorData.getBlob(cursorData.getColumnIndex("photo"));
                Bitmap photo = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);

                Patient patient = new Patient(id, name, number, photo);
                patients.add(patient);

                cursorData.moveToNext();
            }
        }
        cursorData.close();
        database.close();

        return patients;
    }

    private void addPatientsToLayout(List<Patient> patients) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TableRow patientRow = (TableRow) inflater.inflate(R.layout.patient_layout, null);

        if (patients.isEmpty()) {
            TextView textView = new TextView(getActivity());
            TableRow.LayoutParams params = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            params.setMargins(5, 5, 5, 5);
            textView.setLayoutParams(params);
            textView.setText("No Patients to Display");
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(18);
            patientRow.addView(textView);
            baseTable.addView(patientRow);
            return;
        }

        int total = patients.size();
        boolean odd = (total%2 != 0);
        int added = 0;
        for (int a = 0; a < total; a++) {

            Patient patient = patients.get(a);

            TableLayout patientLayout = (TableLayout) inflater.inflate(R.layout.patient_details, null);
            patientLayout.setOnClickListener(new PatientClickListener(patient.getId()));

            ImageView patientPhoto = (ImageView) patientLayout.findViewById(R.id.patientImage);
            patientPhoto.setImageBitmap(patient.getPhoto());

            TextView patientName = (TextView) patientLayout.findViewById(R.id.patientName);
            patientName.setText(patient.getName());

            TextView patientNum = (TextView) patientLayout.findViewById(R.id.patientNumber);
            patientNum.setText(patient.getNumber());

            if (odd && a == total-1) {
                //Last patient of an odd number
                patientLayout.setLayoutParams(singlePatient);
                patientRow.addView(patientLayout);
                baseTable.addView(patientRow);
            } else {
                patientLayout.setLayoutParams(doublePatients);
                patientRow.addView(patientLayout);
                added++;
                if (added == 2) {
                    baseTable.addView(patientRow);
                    patientRow = (TableRow) inflater.inflate(R.layout.patient_layout, null);
                    added = 0;
                }
            }
        }
    }

    private void removePatientsFromBaseView() {
        baseTable.removeViews(1, baseTable.getChildCount()-1);
    }

    private class PatientClickListener implements LinearLayout.OnClickListener {

        private int patientId;

        public PatientClickListener(int patientId) {
            this.patientId = patientId;
        }

        @Override
        public void onClick(View view) {

            PatientDetails patientDetails = new PatientDetails();
            patientDetails.setPatientId(patientId);

            ((MainActivity) getActivity()).updateFragment(patientDetails);
        }
    }
}
