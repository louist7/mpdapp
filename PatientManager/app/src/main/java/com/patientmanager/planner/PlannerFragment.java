package com.patientmanager.planner;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Louis on 22/02/2016.
 */
public class PlannerFragment extends Fragment {

    private TableLayout base;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private Calendar calendar;
    private SimpleDateFormat dateFormatter;
    private Button previous, next;
    private TextView date, completedMessage;
    private String today;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.planner, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        base = (TableLayout) view.findViewById(R.id.base);

        date = (TextView) view.findViewById(R.id.date);
        previous = (Button) view.findViewById(R.id.previousDateBtn);
        next = (Button) view.findViewById(R.id.nextDateBtn);
        setButtonListeners();

        calendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        today = dateFormatter.format(calendar.getTime());
        date.setText("Today");

        addItemsToPlanner(readItemsFromDB(today));

        return view;
    }

    private void setButtonListeners() {
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.DAY_OF_YEAR, -1);

                String current = dateFormatter.format(calendar.getTime());

                removeItemsFromPlanner();
                addItemsToPlanner(readItemsFromDB(current));

                if (current.equals(today)) {
                    date.setText("Today");
                } else {
                    date.setText(current);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendar.add(Calendar.DAY_OF_YEAR, 1);

                String current = dateFormatter.format(calendar.getTime());

                removeItemsFromPlanner();
                addItemsToPlanner(readItemsFromDB(current));

                if (current.equals(today)) {
                    date.setText("Today");
                } else {
                    date.setText(current);
                }
            }
        });
    }

    private List<PlannerItem> readItemsFromDB(String queryDate) {
        List<PlannerItem> items = new ArrayList<PlannerItem>();

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery = "SELECT * FROM planner_items where date = '"+ queryDate +"'";

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            while (!cursorData.isAfterLast()) {

                int id = cursorData.getInt(cursorData.getColumnIndex("itemId"));
                String name = cursorData.getString(cursorData.getColumnIndex("name"));
                String date = cursorData.getString(cursorData.getColumnIndex("date"));
                int patientId = cursorData.getInt(cursorData.getColumnIndex("patientId"));
                boolean done = cursorData.getInt(cursorData.getColumnIndex("done")) > 0;

                PlannerItem item = new PlannerItem();
                item.setItemId(id);
                item.setName(name);
                item.setDate(date);
                item.setPatientId(patientId);
                item.setDone(done);

                items.add(item);

                cursorData.moveToNext();
            }
        }

        cursorData.close();
        database.close();

        return items;
    }

    private void addItemsToPlanner(List<PlannerItem> items) {

        for (PlannerItem item : items) {

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            TableRow itemRow = (TableRow) inflater.inflate(R.layout.planner_item, null);

            TextView name = (TextView) itemRow.findViewById(R.id.itemName);
            name.setText(item.getName());

            CheckBox done = (CheckBox) itemRow.findViewById(R.id.done);
            done.setChecked(item.isDone());
            done.setOnCheckedChangeListener(new DoneClicked(item.getItemId()));

            int patientId = item.getPatientId();
            ImageView patient = (ImageView) itemRow.findViewById(R.id.patient);
            patient.setImageBitmap(getPatientImage(patientId));

            base.addView(itemRow);
        }
        addNewButton();
    }

    private Bitmap getPatientImage(int patientId) {

        Bitmap photo = null;

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery = "SELECT photo FROM patients where patientId = " + patientId;

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            byte[] photoByte = cursorData.getBlob(cursorData.getColumnIndex("photo"));
            photo = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);
        }
        cursorData.close();
        database.close();
        return photo;
    }

    private void removeItemsFromPlanner() {
        base.removeViews(1, base.getChildCount() - 1);
    }

    private void addNewButton() {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TableRow itemRow = (TableRow) inflater.inflate(R.layout.planner_bottom, null);

        Button newButton = (Button) itemRow.findViewById(R.id.newItem);

        completedMessage = (TextView) itemRow.findViewById(R.id.completed);
        setCompletedMessage();

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).updateFragment(new AddItem());
            }
        });

        base.addView(itemRow);
    }

    private void setCompletedMessage() {

        String currentDate = dateFormatter.format(calendar.getTime());

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectAllQuery = "SELECT * FROM planner_items where date = '" + currentDate + "'";
        Cursor cursorAllData = database.rawQuery(selectAllQuery, null);
        int total = cursorAllData.getCount();
        cursorAllData.close();

        String selectQuery = "SELECT * FROM planner_items where done = 1 and date = '" + currentDate + "'";
        Cursor cursorData = database.rawQuery(selectQuery, null);
        int done = cursorData.getCount();
        cursorData.close();

        database.close();

        String message = done + "/" + total + " Completed";

        completedMessage.setText(message);
    }

    private class DoneClicked implements CompoundButton.OnCheckedChangeListener {

        private int itemId;

        public DoneClicked(int id) {
            this.itemId = id;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

            database = databaseHandler.getReadableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put("done", isChecked);

            database.update("planner_items", contentValues, "itemId=" + itemId, null);

            databaseHandler.close();

            setCompletedMessage();
        }
    }
}
