package com.patientmanager.planner;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Louis on 02/03/2016.
 */
public class AddItem extends Fragment {

    private Button save, cancel;
    private EditText name, date;
    private Spinner patients;
    private Calendar calendar;
    private SimpleDateFormat dateFormatter;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private Map<Integer, Integer> patientsInSpinner;
    private TextView noPatients;
    private Drawable originalBackground;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_item, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        patientsInSpinner = new HashMap<Integer, Integer>();

        noPatients = (TextView) view.findViewById(R.id.noPatientsMessage);

        save = (Button) view.findViewById(R.id.saveBtn);
        cancel = (Button) view.findViewById(R.id.cancelBtn);

        name = (EditText) view.findViewById(R.id.itemName);
        patients = (Spinner) view.findViewById(R.id.itemPatients);

        originalBackground = name.getBackground();

        loadPatientsForSpinner();

        date = (EditText) view.findViewById(R.id.itemDate);

        calendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        date.setText(dateFormatter.format(calendar.getTime()));

        setListeners();

        return view;
    }

    private void loadPatientsForSpinner() {


        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        List<String> names = new ArrayList<String>();

        String selectQuery = "SELECT * FROM patients";

        Cursor cursorData = database.rawQuery(selectQuery, null);

        int pos = 0;
        if (cursorData.moveToFirst()) {
            while (!cursorData.isAfterLast()) {

                int id = cursorData.getInt(cursorData.getColumnIndex("patientId"));
                String name = cursorData.getString(cursorData.getColumnIndex("name"));

                names.add(name);
                patientsInSpinner.put(pos, id);

                cursorData.moveToNext();
                pos++;
            }
        }

        String[] items = new String[names.size()];
        items = names.toArray(items);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, items);
        patients.setAdapter(adapter);

        if (names.isEmpty()) {
            noPatients.setVisibility(View.VISIBLE);
            patients.setVisibility(View.GONE);
        }
    }

    private void setListeners() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).hideKeyboard();

                if (validateInput()) {

                    database = databaseHandler.getReadableDatabase();
                    ContentValues contentValues = new ContentValues();

                    contentValues.put("name", name.getText().toString());
                    contentValues.put("date", date.getText().toString());
                    int patientId = patientsInSpinner.get(patients.getSelectedItemPosition());
                    contentValues.put("patientId", patientId);
                    contentValues.put("done", false);

                    database.insert("planner_items", null, contentValues);
                    databaseHandler.close();

                    Toast.makeText(getActivity(), "Planner Item Saved", Toast.LENGTH_SHORT).show();
                    ((MainActivity) getActivity()).updateFragment(new PlannerFragment());
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).updateFragment(new PlannerFragment());
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().setTheme(R.style.PreferencesTheme);
                DatePickerDialog dateDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int mon, int day) {
                        calendar.set(year, mon, day);
                        date.setText(dateFormatter.format(calendar.getTime()));
                        getActivity().setTheme(R.style.AppTheme);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                dateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().setTheme(R.style.AppTheme);
                    }
                });
                dateDialog.show();
            }
        });
    }

    private Boolean validateInput() {

        if (name.getText().length() > 0) {
            name.setBackground(originalBackground);
        } else {
            name.setBackground(getResources().getDrawable(R.drawable.edittext_error));
        }

        return (name.getText().length() > 0) &&
                (patients.getSelectedItem() != null);
    }

    public void backButtonPressed() {
        ((MainActivity) getActivity()).updateFragment(new PlannerFragment());
    }
}
