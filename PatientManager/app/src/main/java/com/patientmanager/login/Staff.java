package com.patientmanager.login;

/**
 * Created by Louis on 02/03/2016.
 */
public class Staff {

    private int staffId;
    private String staffNo;
    private String password;

    public Staff(String number, String pass) {
        this.staffNo = number;
        this.password = pass;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }
}
