package com.patientmanager.login;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;
import com.patientmanager.patients.PatientScreenFragment;

/**
 * Created by Louis on 02/03/2016.
 */
public class Login extends Fragment {

    private Button login;
    private EditText staff_no, password;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private TextView invalidMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        staff_no = (EditText) view.findViewById(R.id.staffNo);
        password = (EditText) view.findViewById(R.id.password);
        invalidMessage = (TextView) view.findViewById(R.id.invalidMessage);

        login = (Button) view.findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkLogin(staff_no.getText().toString(), password.getText().toString())) {
                    ((MainActivity) getActivity()).drawMenu();
                    ((MainActivity) getActivity()).addToolbar();
                    ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
                } else {
                      invalidMessage.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }

    private boolean checkLogin(String no, String pass) {

        database = databaseHandler.getWritableDatabase();

        String selectQuery = "SELECT * FROM staff where staff_no = '" + no + "' and password = '" + pass +"'" ;
        Cursor cursorData = database.rawQuery(selectQuery, null);

        boolean found = (cursorData.getCount() > 0);

        cursorData.close();
        database.close();

        return found;
    }
}
