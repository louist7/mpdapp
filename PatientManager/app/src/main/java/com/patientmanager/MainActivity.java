package com.patientmanager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.patientmanager.database.InitDB;
import com.patientmanager.notes.AddEditNote;
import com.patientmanager.notes.AddEditReminder;
import com.patientmanager.notes.PatientNotes;
import com.patientmanager.notes.StaffReminders;
import com.patientmanager.patients.AddEditPatient;
import com.patientmanager.patients.PatientDetails;
import com.patientmanager.patients.PatientScreenFragment;
import com.patientmanager.planner.AddItem;
import com.patientmanager.planner.PlannerFragment;
import com.patientmanager.settings.SettingsFragment;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateFragment(new InitDB());
    }

    public void addToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Drawable icon = ContextCompat.getDrawable(getApplicationContext(), R.drawable.menu);
        Bitmap b = ((BitmapDrawable)icon).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 70, 70, false);
        icon = new BitmapDrawable(getResources(), bitmapResized);

        toolbar.setNavigationIcon(icon);
        toolbar.setNavigationOnClickListener(new MenuToggleListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionSettings) {
            launchSettings();
        }

        return super.onOptionsItemSelected(item);
    }

    public void drawMenu() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        String[] features = { "My Patients", "My Tasks", "Settings"};
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, features);
        mDrawerList = (ListView) findViewById(R.id.leftDrawer);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.bringToFront();
        mDrawerList.setOnItemClickListener(new MenuItemClickListener());
    }

    private void launchSettings() {
        updateFragment(new SettingsFragment());
    }

    public void updateFragment(Fragment fragment) {
        hideKeyboard();

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment, "FRAGMENT_TAG");
        fragmentTransaction.commit();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            return;
        }
        else {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        final Fragment fragment = (Fragment) fragmentManager.findFragmentByTag("FRAGMENT_TAG");

        hideKeyboard();
        if (fragment instanceof AddEditPatient) {
            ((AddEditPatient) fragment).backButtonPressed();
        } else if (fragment instanceof PatientDetails) {
            ((PatientDetails) fragment).backButtonPressed();
        } else if (fragment instanceof PatientNotes) {
            ((PatientNotes) fragment).backButtonPressed();
        } else if (fragment instanceof AddEditNote) {
            ((AddEditNote) fragment).backButtonPressed();
        } else if (fragment instanceof StaffReminders) {
            ((StaffReminders) fragment).backButtonPressed();
        } else if (fragment instanceof AddEditReminder) {
            ((AddEditReminder) fragment).backButtonPressed();
        } else if (fragment instanceof AddItem) {
            ((AddItem) fragment).backButtonPressed();
        } else {
            super.onBackPressed();
        }
    }

    private class MenuItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    updateFragment(new PatientScreenFragment());
                    break;
                case 1:
                    updateFragment(new PlannerFragment());
                    break;
                case 2:
                    updateFragment(new SettingsFragment());
                    break;
            }
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    private class MenuToggleListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            hideKeyboard();
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
    }
}
