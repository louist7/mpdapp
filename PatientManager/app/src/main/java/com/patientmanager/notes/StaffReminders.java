package com.patientmanager.notes;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;
import com.patientmanager.patients.PatientDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Louis on 28/02/2016.
 */
public class StaffReminders extends Fragment {

    private int patientId;
    private String patientName;
    private TableLayout baseReminders;
    private Button addNew;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private EditText search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.staff_reminders, container, false);

        baseReminders = (TableLayout) view.findViewById(R.id.baseReminders);

        addNew = (Button) view.findViewById(R.id.addReminderBtn);

        setButtonListeners();

        addRemindersToLayout(readRemindersFromDB(null));

        search = (EditText) view.findViewById(R.id.searchReminders);

        search.setHint(Html.fromHtml("<small> Search " + patientName + "'s Reminders</small>"));
        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    ((MainActivity) getActivity()).hideKeyboard();
                    addRemindersToLayout(readRemindersFromDB((v.getText()).toString()));
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private List<Reminder> readRemindersFromDB(String contentFilter) {

        List<Reminder> reminders = new ArrayList<Reminder>();

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery;

        if (contentFilter == null) {
            selectQuery = "SELECT * FROM reminders where patientId =" + patientId;
        } else {
            removeAllRemindersFromLayout();
            selectQuery = "SELECT * FROM reminders where patientId =" + patientId + " and content like '%" + contentFilter + "%'";
        }

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            while (!cursorData.isAfterLast()) {

                int reminderId = cursorData.getInt(cursorData.getColumnIndex("reminderId"));
                String content = cursorData.getString(cursorData.getColumnIndex("content"));
                String description = cursorData.getString(cursorData.getColumnIndex("description"));

                Reminder reminder = new Reminder();
                reminder.setReminderId(reminderId);
                reminder.setContent(content);
                reminder.setDescription(description);
                reminder.setPatientId(patientId);

                reminders.add(reminder);

                cursorData.moveToNext();
            }
        }
        cursorData.close();
        database.close();

        return reminders;
    }

    private void removeAllRemindersFromLayout() {

        baseReminders.removeViews(1, baseReminders.getChildCount() - 1);
    }

    private void addRemindersToLayout(List<Reminder> reminders) {

        if (reminders.isEmpty()) {
            TextView textView = new TextView(getActivity());
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.setMargins(20, 60, 0, 0);
            textView.setLayoutParams(params);
            textView.setText("No Reminders to Display");
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(20);
            baseReminders.addView(textView);
            return;
        }

        for (final Reminder reminder : reminders) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            TableRow patientReminders = (TableRow) inflater.inflate(R.layout.reminders_details, null);

            TextView remindersContent = (TextView) patientReminders.findViewById(R.id.reminderContent);
            remindersContent.setText(reminder.getContent());

            TextView reminderDesc = (TextView) patientReminders.findViewById(R.id.reminderDesc);
            reminderDesc.setText(reminder.getDescription());
            reminderDesc.setMovementMethod(new ScrollingMovementMethod());

            LinearLayout layout = (LinearLayout) patientReminders.findViewById(R.id.linear);
            layout.setOnClickListener(new ReminderClickListener(reminderDesc));

            ImageButton edit = (ImageButton) patientReminders.findViewById(R.id.editBtn);
            ImageButton delete = (ImageButton) patientReminders.findViewById(R.id.deleteBtn);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AddEditReminder addEditReminder = new AddEditReminder();
                    addEditReminder.setPatientDetails(patientId, patientName);
                    addEditReminder.setEditing(true);
                    addEditReminder.setReminder(reminder);
                    ((MainActivity) getActivity()).updateFragment(addEditReminder);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().setTheme(R.style.PreferencesTheme);
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

                    dialog.setTitle("Delete Reminder");
                    dialog.setMessage("Are you sure you want to delete this note?");
                    dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().setTheme(R.style.AppTheme);
                            deleteReminder(reminder);
                        }
                    });
                    dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().setTheme(R.style.AppTheme);
                        }
                    });
                    dialog.show();
                }
            });

            baseReminders.addView(patientReminders);
        }
    }

    private void deleteReminder(Reminder reminder){

        database = databaseHandler.getReadableDatabase();
        database.delete("reminders", "reminderId=" + reminder.getReminderId(), null);
        databaseHandler.close();

        Toast.makeText(getActivity(), "Reminder Deleted", Toast.LENGTH_SHORT).show();

        StaffReminders reminders = new StaffReminders();
        reminders.setPatientDetails(patientId, patientName);
        ((MainActivity) getActivity()).updateFragment(reminders);
    }

    private void setButtonListeners() {
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddEditReminder addEditReminder = new AddEditReminder();
                addEditReminder.setPatientDetails(patientId, patientName);
                addEditReminder.setEditing(false);
                ((MainActivity) getActivity()).updateFragment(addEditReminder);
            }
        });
    }

    public void setPatientDetails(int id, String patientName) {
        this.patientId = id;
        this.patientName = patientName;
    }

    public void backButtonPressed() {

        PatientDetails patientDetails = new PatientDetails();
        patientDetails.setPatientId(patientId);
        ((MainActivity) getActivity()).updateFragment(patientDetails);
    }

    private class ReminderClickListener implements LinearLayout.OnClickListener {

        private TextView desc;

        public ReminderClickListener(TextView desc) {
            this.desc = desc;
        }

        @Override
        public void onClick(View view) {

            boolean hidden = (desc.getVisibility() == View.GONE);

            if (hidden) {
                desc.setVisibility(View.VISIBLE);
            } else {
                desc.setVisibility(View.GONE);
            }
        }
    }
}
