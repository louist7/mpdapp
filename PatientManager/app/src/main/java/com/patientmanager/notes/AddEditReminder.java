package com.patientmanager.notes;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

/**
 * Created by Louis on 29/02/2016.
 */
public class AddEditReminder extends Fragment {

    private int patientId;
    private String patientName;
    private Button save, cancel;
    private EditText content, description;
    private TextView name;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private boolean editing;
    private Reminder reminder;
    private Drawable originalBackground;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_edit_reminder, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        save = (Button) view.findViewById(R.id.saveBtn);
        cancel = (Button) view.findViewById(R.id.cancelBtn);

        content = (EditText) view.findViewById(R.id.reminderContent);
        description = (EditText) view.findViewById(R.id.reminderDesc);

        name = (TextView) view.findViewById(R.id.name);

        originalBackground = content.getBackground();

        setButtonListeners();

        renderDetailsOnScreen();

        return view;
    }

    private void renderDetailsOnScreen() {

        if (editing) {
            content.setText(reminder.getContent());
            description.setText(reminder.getDescription());
            name.setText("Edit Reminder for " + patientName);
        } else {
            name.setText("Add Reminder for " + patientName);
        }
    }

    private void setButtonListeners() {

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).hideKeyboard();

                if (validateInput()) {

                    database = databaseHandler.getReadableDatabase();

                    ContentValues contentValues = new ContentValues();

                    contentValues.put("patientId", patientId);
                    contentValues.put("content", content.getText().toString());
                    contentValues.put("description", description.getText().toString());

                    if (editing) {
                        database.update("reminders", contentValues, "reminderId=" + reminder.getReminderId(), null);
                    } else {
                        database.insert("reminders", null, contentValues);
                    }

                    databaseHandler.close();

                    Toast.makeText(getActivity(), "Staff Reminder Saved", Toast.LENGTH_SHORT).show();

                    StaffReminders reminders = new StaffReminders();
                    reminders.setPatientDetails(patientId, patientName);
                    ((MainActivity) getActivity()).updateFragment(reminders);
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     ((MainActivity) getActivity()).hideKeyboard();

                     StaffReminders reminders = new StaffReminders();
                     reminders.setPatientDetails(patientId, patientName);
                     ((MainActivity) getActivity()).updateFragment(reminders);
            }
        });
    }

    private boolean validateInput() {

        if (content.getText().length() > 0) {
            content.setBackground(originalBackground);
        } else {
            content.setBackground(getResources().getDrawable(R.drawable.edittext_error));
        }

        return (content.getText().length() > 0);
    }

    public void backButtonPressed() {
        StaffReminders reminders = new StaffReminders();
        reminders.setPatientDetails(patientId, patientName);
        ((MainActivity) getActivity()).updateFragment(reminders);
    }

    public void setPatientDetails(int id, String patientName) {
        this.patientId = id;
        this.patientName = patientName;
    }

    public void setEditing(boolean edit) {
        this.editing = edit;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }
}
