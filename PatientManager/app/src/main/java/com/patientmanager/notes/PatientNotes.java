package com.patientmanager.notes;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;
import com.patientmanager.patients.PatientDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Louis on 28/02/2016.
 */
public class PatientNotes extends Fragment {

    private int patientId;
    private String patientName;
    private TableLayout baseNotes;
    private Button addNew;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private EditText search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.patient_notes, container, false);

        baseNotes = (TableLayout) view.findViewById(R.id.baseNotes);

        addNew = (Button) view.findViewById(R.id.addNoteBtn);

        setButtonListeners();

        addNotesToLayout(readNotesFromDB(null));

        search = (EditText) view.findViewById(R.id.searchNotes);

        search.setHint(Html.fromHtml("<small> Search " + patientName + "'s Notes</small>"));
        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    ((MainActivity) getActivity()).hideKeyboard();
                    addNotesToLayout(readNotesFromDB((v.getText()).toString()));
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private List<Note> readNotesFromDB(String titleFilter) {

        List<Note> notes = new ArrayList<Note>();

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();

        String selectQuery;

        if (titleFilter == null) {
            selectQuery = "SELECT * FROM notes where patientId =" + patientId;
        } else {
            removeAllNotesFromLayout();
            selectQuery = "SELECT * FROM notes where patientId =" + patientId + " and title like '%" + titleFilter + "%'";
        }

        Cursor cursorData = database.rawQuery(selectQuery, null);

        if (cursorData.moveToFirst()) {
            while (!cursorData.isAfterLast()) {

                int noteId = cursorData.getInt(cursorData.getColumnIndex("noteId"));
                String title = cursorData.getString(cursorData.getColumnIndex("title"));
                String content = cursorData.getString(cursorData.getColumnIndex("content"));

                Note note = new Note();
                note.setNoteId(noteId);
                note.setTitle(title);
                note.setContent(content);
                notes.add(note);

                cursorData.moveToNext();
            }
        }
        cursorData.close();
        database.close();

        return notes;
    }

    private void removeAllNotesFromLayout() {

        baseNotes.removeViews(1, baseNotes.getChildCount() - 1);
    }

    private void addNotesToLayout(List<Note> notes) {

        if (notes.isEmpty()) {
            TextView textView = new TextView(getActivity());
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.setMargins(20, 60, 0, 0);
            textView.setLayoutParams(params);
            textView.setText("No Notes to Display");
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(20);
            baseNotes.addView(textView);
            return;
        }

        for (final Note note : notes) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            TableRow patientNote = (TableRow) inflater.inflate(R.layout.notes_details, null);

            TextView notesTitle = (TextView) patientNote.findViewById(R.id.noteTitle);
            notesTitle.setText(note.getTitle());

            TextView notesDesc = (TextView) patientNote.findViewById(R.id.noteDesc);
            notesDesc.setText(note.getContent());
            notesDesc.setMovementMethod(new ScrollingMovementMethod());

            LinearLayout layout = (LinearLayout) patientNote.findViewById(R.id.linear);
            layout.setOnClickListener(new NoteClickListener(notesDesc));

            ImageButton edit = (ImageButton) patientNote.findViewById(R.id.editBtn);
            ImageButton delete = (ImageButton) patientNote.findViewById(R.id.deleteBtn);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddEditNote addEditNote = new AddEditNote();
                    addEditNote.setPatientDetails(patientId, patientName);
                    addEditNote.setEditing(true);
                    addEditNote.setNote(note);
                    ((MainActivity) getActivity()).updateFragment(addEditNote);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().setTheme(R.style.PreferencesTheme);
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

                    dialog.setTitle("Delete Note");
                    dialog.setMessage("Are you sure you want to delete this note?");
                    dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().setTheme(R.style.AppTheme);
                            deleteNote(note);
                        }
                    });
                    dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().setTheme(R.style.AppTheme);
                        }
                    });
                    dialog.show();
                }
            });

            baseNotes.addView(patientNote);
        }
    }

    private void deleteNote(Note note){

        database = databaseHandler.getReadableDatabase();
        database.delete("notes", "noteId=" + note.getNoteId(), null);
        databaseHandler.close();

        Toast.makeText(getActivity(), "Note Deleted", Toast.LENGTH_SHORT).show();

        PatientNotes notes = new PatientNotes();
        notes.setPatientDetails(patientId, patientName);
        ((MainActivity) getActivity()).updateFragment(notes);
    }

    private void setButtonListeners() {
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddEditNote addEditNote = new AddEditNote();
                addEditNote.setPatientDetails(patientId, patientName);
                addEditNote.setEditing(false);
                ((MainActivity) getActivity()).updateFragment(addEditNote);
            }
        });
    }

    public void setPatientDetails(int id, String patientName) {
        this.patientId = id;
        this.patientName = patientName;
    }

    public void backButtonPressed() {

        PatientDetails patientDetails = new PatientDetails();
        patientDetails.setPatientId(patientId);
        ((MainActivity) getActivity()).updateFragment(patientDetails);
    }

    private class NoteClickListener implements LinearLayout.OnClickListener {

        private TextView desc;

        public NoteClickListener(TextView desc) {
            this.desc = desc;
        }

        @Override
        public void onClick(View view) {

            boolean hidden = (desc.getVisibility() == View.GONE);

            if (hidden) {
                desc.setVisibility(View.VISIBLE);
            } else {
                desc.setVisibility(View.GONE);
            }
        }
    }
}
