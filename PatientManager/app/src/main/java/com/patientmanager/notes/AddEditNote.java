package com.patientmanager.notes;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;

/**
 * Created by Louis on 29/02/2016.
 */
public class AddEditNote extends Fragment {

    private int patientId;
    private String patientName;
    private Button save, cancel;
    private EditText title, content;
    private TextView name;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;
    private boolean editing;
    private Note note;
    private Drawable originalBackground;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_edit_note, container, false);

        databaseHandler = new DatabaseHandler(getActivity());

        save = (Button) view.findViewById(R.id.saveBtn);
        cancel = (Button) view.findViewById(R.id.cancelBtn);

        title = (EditText) view.findViewById(R.id.noteTitle);
        content = (EditText) view.findViewById(R.id.noteContent);

        name = (TextView) view.findViewById(R.id.name);

        originalBackground = title.getBackground();

        setButtonListeners();

        renderDetailsOnScreen();

        return view;
    }

    private void renderDetailsOnScreen() {

        if (editing) {
            title.setText(note.getTitle());
            content.setText(note.getContent());
            name.setText("Edit Note for " + patientName);
        } else {
            name.setText("Add Note for " + patientName);
        }
    }

    private void setButtonListeners() {

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).hideKeyboard();

                if (validateInput()) {

                    database = databaseHandler.getReadableDatabase();

                    ContentValues contentValues = new ContentValues();

                    contentValues.put("patientId", patientId);
                    contentValues.put("title", title.getText().toString());
                    contentValues.put("content", content.getText().toString());

                    if (editing) {
                        database.update("notes", contentValues, "noteId=" + note.getNoteId(), null);
                    } else {
                        database.insert("notes", null, contentValues);
                    }

                    databaseHandler.close();

                    Toast.makeText(getActivity(), "Patient Note Saved", Toast.LENGTH_SHORT).show();

                    PatientNotes notes = new PatientNotes();
                    notes.setPatientDetails(patientId, patientName);
                    ((MainActivity) getActivity()).updateFragment(notes);
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     ((MainActivity) getActivity()).hideKeyboard();

                     PatientNotes notes = new PatientNotes();
                     notes.setPatientDetails(patientId, patientName);
                     ((MainActivity) getActivity()).updateFragment(notes);
            }
        });
    }

    private boolean validateInput() {

        if (title.getText().length() > 0) {
            title.setBackground(originalBackground);
        } else {
            title.setBackground(getResources().getDrawable(R.drawable.edittext_error));
        }

        return (title.getText().length() > 0);
    }

    public void backButtonPressed() {
        PatientNotes notes = new PatientNotes();
        notes.setPatientDetails(patientId, patientName);
        ((MainActivity) getActivity()).updateFragment(notes);
    }

    public void setPatientDetails(int id, String patientName) {
        this.patientId = id;
        this.patientName = patientName;
    }

    public void setEditing(boolean edit) {
        this.editing = edit;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
