package com.patientmanager.settings;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.database.DatabaseHandler;
import com.patientmanager.login.Staff;

/**
 * Created by Louis on 22/02/2016.
 */
public class SettingsFragment extends PreferenceFragment {

    private SharedPreferences.OnSharedPreferenceChangeListener listener;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;

    private Preference update;

    private static String LOGIN_LOCK = "login_lock";
    private static String UPDATE = "update";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTheme(R.style.PreferencesTheme);
        addPreferencesFromResource(R.xml.settings);

        databaseHandler = new DatabaseHandler(getActivity());

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {

            @Override
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

                if (key.equals(LOGIN_LOCK)) {
                    String value = prefs.getString(key, "");
                    if (value.equals("No")) {
                        //do nothing
                    } else if (value.equals("Yes")) {
                        if (checkStaffExists()) {
                            // do nothing
                        } else {
                            // create the dialog which will persist a user
                            setupDialog(false);
                        }
                    }
                }

                //update preference summary
                Preference connectionPref = findPreference(key);
                connectionPref.setSummary(prefs.getString(key, ""));
            }
        };
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);

        update = getPreferenceScreen().findPreference(UPDATE);
        update.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setupDialog(true);
                return true;
            }
        });
    }

    private boolean checkStaffExists() {

        database = databaseHandler.getReadableDatabase();
        String selectQuery = "SELECT * FROM staff";

        Cursor cursorData = database.rawQuery(selectQuery, null);

        boolean found = !(cursorData.getCount() == 0);

        cursorData.close();
        database.close();

        return found;
    }

    private void setupDialog(final boolean updating) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Enter Your Credentials");

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText staffNo = new EditText(getActivity());
        staffNo.setHint("Staff Number");
        staffNo.setInputType(InputType.TYPE_CLASS_NUMBER);
        staffNo.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        final EditText password = new EditText(getActivity());
        password.setHint("Password");
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        password.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        layout.addView(staffNo);
        layout.addView(password);

        alert.setView(layout);

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String staffValue = staffNo.getText().toString();
                String passwordValue = password.getText().toString();

                if (staffValue.length() > 0 && passwordValue.length() > 0) {
                    ((MainActivity) getActivity()).hideKeyboard();

                    Staff staff = new Staff(staffValue, passwordValue);
                    updateStaffDB(staff);
                    Toast.makeText(getActivity(), "Credentials Saved", Toast.LENGTH_SHORT).show();
                } else {
                    ((MainActivity) getActivity()).hideKeyboard();

                    if (!updating) {
                        setLoginLockNo();
                        ((MainActivity) getActivity()).updateFragment(new SettingsFragment());
                    }
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (!updating) {
                    setLoginLockNo();
                    ((MainActivity) getActivity()).updateFragment(new SettingsFragment());
                }
            }
        });

        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (!updating) {
                    setLoginLockNo();
                    ((MainActivity) getActivity()).updateFragment(new SettingsFragment());
                }
            }
        });

        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                ((MainActivity) getActivity()).hideKeyboard();
            }
        });

        alert.show();
    }

    private void setLoginLockNo() {
        SharedPreferences prefs = getPreferenceScreen().getSharedPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOGIN_LOCK, "No");
        editor.commit();
    }

    private void updateStaffDB(Staff staff) {

        deleteStaff();

        database = databaseHandler.getReadableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put("staff_no", staff.getStaffNo());
        contentValues.put("password", staff.getPassword());

        database.insert("staff", null, contentValues);

        database.close();
    }

    private void deleteStaff() {
        database = databaseHandler.getReadableDatabase();
        String delete = "DELETE FROM staff";

        database.execSQL(delete);

        databaseHandler.close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().setTheme(R.style.AppTheme);
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
    }
}
