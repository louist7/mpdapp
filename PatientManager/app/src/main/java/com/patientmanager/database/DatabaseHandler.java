package com.patientmanager.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Louis on 22/02/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    public static final Integer VERSION = 1;
    public static final String DB_NAME = "patientmanager.db";

    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String createStaff = "CREATE TABLE staff " +
                "(staffId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "staff_no VARCHAR(100)," +
                "password VARCHAR(100))";

        String createPatients = "CREATE TABLE patients" +
                "(patientId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR(50)," +
                "dob VARCHAR(10)," +
                "address VARCHAR(100)," +
                "number VARCHAR(50)," +
                "nhs_number VARCHAR(12)," +
                "bio VARCHAR(100)," +
                "photo blob)";

        String createNotes = "CREATE TABLE notes " +
                "(noteId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "patientId INTEGER," +
                "title VARCHAR(100)," +
                "content VARCHAR(255))";

        String createReminders = "CREATE TABLE reminders " +
                "(reminderId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "patientId INTEGER," +
                "content VARCHAR(100)," +
                "description VARCHAR(255))";

        String createPlannerItems = "CREATE TABLE planner_items " +
                "(itemId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR(100)," +
                "date VARCHAR(20)," +
                "patientId INTEGER," +
                "done boolean)";

        database.execSQL(createStaff);
        database.execSQL(createPatients);
        database.execSQL(createNotes);
        database.execSQL(createReminders);
        database.execSQL(createPlannerItems);
    }

    @Override
    public void onUpgrade (SQLiteDatabase database, int oldVersion, int newVersion) {

        String dropStaff = "DROP TABLE IF EXISTS staff";
        String dropPatients = "DROP TABLE IF EXISTS patients";
        String dropNotes = "DROP TABLE IF EXISTS notes";
        String dropReminders = "DROP TABLE IF EXISTS reminders";
        String dropPlannerItems = "DROP TABLE IF EXISTS planner_items";

        database.execSQL(dropStaff);
        database.execSQL(dropPatients);
        database.execSQL(dropNotes);
        database.execSQL(dropReminders);
        database.execSQL(dropPlannerItems);

        onCreate(database);
    }

    @Override
    public synchronized void close() {
        super.close();
    }
}
