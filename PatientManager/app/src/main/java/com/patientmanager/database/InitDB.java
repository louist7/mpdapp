package com.patientmanager.database;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.patientmanager.MainActivity;
import com.patientmanager.R;
import com.patientmanager.login.Login;
import com.patientmanager.patients.PatientScreenFragment;


/**
 * Created by Louis on 02/03/2016.
 */
public class InitDB extends Fragment {

    private SharedPreferences settings;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase database;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.patient_screen, container, false);

        databaseHandler = new DatabaseHandler(getActivity());
        database = databaseHandler.getReadableDatabase();
        database.close();

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String autoLogin = settings.getString("login_lock", "No");

        if (autoLogin.equals("Yes")) {
            ((MainActivity) getActivity()).updateFragment(new Login());
        } else {
            ((MainActivity) getActivity()).drawMenu();
            ((MainActivity) getActivity()).addToolbar();
            ((MainActivity) getActivity()).updateFragment(new PatientScreenFragment());
        }

        return view;
    }
}
